#include <string>
#include <vector>

using namespace std;



class char_generic {

private:  
	string name, race, class_specific;
	vector<int> abil_score;
	
public:
	char_generic();
	
	char_generic(string _name, string _race, string _class_specific, vector<int> _abil_score) : 
		name(_name), race(_race), class_specific(_class_specific), abil_score(_abil_score) {};
	

};